package SingleInstance

import (
	"fmt"
	"github.com/fatih/color"
	"os"
	"path/filepath"
	"syscall"
	"time"
	"unsafe"
)

var (
	kernel32        = syscall.NewLazyDLL("kernel32.dll")
	procCreateMutex = kernel32.NewProc("CreateMutexW")
)

func createMutex(name string) (uintptr, error) {

	result, err := syscall.UTF16PtrFromString(name)

	ret, _, err := procCreateMutex.Call(
		0,
		0,
		uintptr(unsafe.Pointer(result)),
	)
	switch int(err.(syscall.Errno)) {
	case 0:
		return ret, nil
	default:
		return ret, err
	}
}

func Activate() {
	// mutexName starting with "Global\" will work across all user sessions

	filename := filepath.Base(os.Args[0])
	_, err := createMutex(filename)
	fmt.Println("-------------------")
	fmt.Println(os.Args[0])
	fmt.Println("---------------")

	if err != nil {
		color.Red("error app already running...")
		time.Sleep(5 * time.Second)
		os.Exit(1)
	}

}
