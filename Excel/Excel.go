package Excel

import (
	"errors"
	"fmt"
	"github.com/360EntSecGroup-Skylar/excelize"
	"strconv"
)

type CellRef struct {
	Col string
	Row int64
}

func NewCellRef(col string, row int64) *CellRef {
	return &CellRef{Col: col, Row: row}
}

func (c *CellRef) IncrementCol() (*CellRef, error) {
	alpha := []string{"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"}

	for i, s := range alpha {
		if c.Col == s && i+1 != len(alpha) {

			c.Col = alpha[i+1]
			return c, nil
		}

	}
	return nil, errors.New("only 26 cols allowed")
}

func (c *CellRef) IncrementRow() *CellRef {
	c.Row++
	return c
}

func (c CellRef) String() string {
	return string(c.Col + strconv.FormatInt(c.Row, 10))
}

type Somsi []map[string]interface{}

func (sm *Somsi) Add(m map[string]interface{}) {

	*sm = append(*sm, m)
}

func (sm Somsi) ToXlsx(cols []string, startcell *CellRef) *excelize.File {
	//create new xlsx file
	xlsx := excelize.NewFile()

	currentcell := NewCellRef(startcell.Col, startcell.Row)

	// set coloumn titles
	for _, col := range cols {

		s2 := currentcell.String()
		xlsx.SetCellValue("Sheet1", s2, col)

		currentcell.IncrementCol()

	}

	//reset col and increment row
	currentcell = NewCellRef(startcell.Col, startcell.Row)
	currentcell.IncrementRow()

	for _, row := range sm {
		for _, col := range cols {
			value := row[col]

			if value == nil {
				value = ""
			}
			xlsx.SetCellValue("Sheet1", currentcell.String(), fmt.Sprintf("%v", value))
			//increment col
			currentcell.IncrementCol()
		}
		currentcell = NewCellRef(startcell.Col, currentcell.Row)
		currentcell.IncrementRow()
	}

	return xlsx
}
