module gitlab.com/dafabe/toolbox

go 1.13

require (
	github.com/360EntSecGroup-Skylar/excelize v1.4.1
	github.com/DATA-DOG/go-sqlmock v1.5.0
	github.com/fatih/color v1.7.0
	github.com/fsnotify/fsnotify v1.4.7
	github.com/go-sql-driver/mysql v1.4.1
	github.com/mattn/go-colorable v0.1.2 // indirect
	github.com/mattn/go-isatty v0.0.9 // indirect
	github.com/sirupsen/logrus v1.4.2
	google.golang.org/appengine v1.6.2 // indirect
)
