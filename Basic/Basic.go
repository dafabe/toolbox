package Basic

import (
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/fatih/color"
	"github.com/go-sql-driver/mysql"
	_ "github.com/go-sql-driver/mysql"
	"log"
	"os"
	"reflect"
	"strings"
)

// NullInt64 is an alias for sql.NullInt64 data type
type NullInt64 sql.NullInt64

// NullBool is an alias for sql.NullBool data type
type NullBool sql.NullBool

// NullFloat64 is an alias for sql.NullFloat64 data type
type NullFloat64 sql.NullFloat64

// NullString is an alias for sql.NullString data type
type NullString sql.NullString

// NullTime is an alias for mysql.NullTime data type
type NullTime mysql.NullTime

// UnmarshalJSON for NullString
func (ns *NullString) UnmarshalJSON(b []byte) error {
	err := json.Unmarshal(b, &ns.String)
	ns.Valid = (err == nil)
	return err
}

/*
type Flatener interface {
	Flatenen()
}*/
// UnmarshalJSON for NullString
func (ns *NullString) Flatten(a string) string {
	return ns.String

}

// Scan implements the Scanner interface for NullInt64
func (ni *NullString) Scan(value interface{}) error {
	var i sql.NullString
	if err := i.Scan(value); err != nil {
		return err
	}
	// if nil the make Valid false
	if reflect.TypeOf(value) == nil {
		*ni = NullString{i.String, false}
	} else {
		*ni = NullString{i.String, true}
	}
	return nil
}

func (ni *NullFloat64) Scan(value interface{}) error {
	var i sql.NullFloat64
	if err := i.Scan(value); err != nil {
		return err
	}
	// if nil the make Valid false
	if reflect.TypeOf(value) == nil {
		*ni = NullFloat64{i.Float64, false}
	} else {
		*ni = NullFloat64{i.Float64, true}
	}
	return nil
}

func (ni *NullInt64) Scan(value interface{}) error {
	var i sql.NullInt64
	if err := i.Scan(value); err != nil {
		return err
	}
	// if nil the make Valid false
	if reflect.TypeOf(value) == nil {
		*ni = NullInt64{i.Int64, false}
	} else {
		*ni = NullInt64{i.Int64, true}
	}
	return nil
}
func QueryToJson(cs string, query string, args ...interface{}) ([]byte, error) {

	data, err := QueryToSliceOfMaps(cs, query, args...)
	if err != nil {
		fmt.Print(err)
	}
	return json.MarshalIndent(data, "", "\t")

}

func InterfaceToString(input interface{}) string {

	return fmt.Sprintf("%v", input)
}
func QueryToMapOfMaps(key, cs, query string, args ...interface{}) (map[string]map[string]interface{}, error) {

	data, err := QueryToSliceOfMaps(cs, query, args...)
	if err != nil {
		return nil, err
	}

	mapOfMap := make(map[string]map[string]interface{})

	for _, value := range data {

		s := InterfaceToString(value[key])
		mapOfMap[s] = value

	}

	if len(mapOfMap) == 0 {
		return mapOfMap, errors.New("Error No Records Found")
	}
	return mapOfMap, nil

}

type DataGrid []map[string]interface{}

type IQueryInfo interface {
}

type QueryInfo struct {
	ConnectionString string
	Query            string
	Args             []interface{}
}

func (qi QueryInfo) GetData() ([]map[string]interface{}, error) {

	m, err := QueryToSliceOfMaps(qi.ConnectionString, qi.Query, qi.Args...)

	return m, err
}

func GetDataStub() ([]map[string]interface{}, error) {

	sm := make([]map[string]interface{}, 3)

	sm = append(sm, map[string]interface{}{"name": "Sue", "age": 25})

	sm = append(sm, map[string]interface{}{"name": "Bob", "age": 55})

	return sm, nil

}

func (qi QueryInfo) QueryToDataGrid(action func() ([]map[string]interface{}, error)) ([]map[string]interface{}, error) {

	return action()
}

func QueryToSliceOfMapsCon(db *sql.DB, query string, args ...interface{}) ([]map[string]interface{}, error) {
	// an array of JSON objects
	// the map key is the field name

	var objects []map[string]interface{}

	rows, err := db.Query(query, args...)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		// figure out what columns were returned
		// the column names will be the JSON object field keys
		columns, err := rows.ColumnTypes()
		if err != nil {
			return nil, err
		}

		// Scan needs an array of pointers to the values it is setting
		// This creates the object and sets the values correctly
		//
		values := make([]interface{}, len(columns))
		object := map[string]interface{}{}
		for i, column := range columns {
			var v interface{}

			switch column.DatabaseTypeName() {
			case "DECIMAL":
				v = new(sql.NullString)
			case "VARCHAR":
				v = new(sql.NullString)
			case "CHAR":
				v = new(sql.NullString)
			case "BIGINT":
				v = new(sql.NullInt64)
			case "SMALLINT":
				v = new(sql.NullInt64)
			case "DATE":
				v = new(sql.NullString)
			case "TEXT":
				v = new(sql.NullString)
			case "LONGTEXT":
				v = new(sql.NullString)
			case "DATETIME":
				v = new(sql.NullString)
			case "INT":
				v = new(sql.NullInt64)
			case "VARBINARY":
				v = new(sql.NullString)
			case "DOUBLE":
				v = new(sql.NullString)
			case "MEDIUMINT":
				v = new(sql.NullInt64)
			default:
				v = new(interface{}) // destination must be a pointer

				// use to figure out types for columns
				log.Println(column.Name(), column.DatabaseTypeName())

			}

			object[column.Name()] = v

			values[i] = v

		}

		err = rows.Scan(values...)

		if err != nil {
			return nil, err
		}

		row := map[string]interface{}{}

		for k := range object {

			row[k] = ConvertBackToStandardType(object[k])
		}

		objects = append(objects, row)
	}

	if len(objects) == 0 {
		return objects, errors.New("Error No Records Found")
	}

	return objects, err
}
func QueryToSliceOfMaps(cs string, query string, args ...interface{}) ([]map[string]interface{}, error) {
	// an array of JSON objects
	// the map key is the field name

	db, err := sql.Open("mysql", cs)
	if err != nil {
		color.Red(err.Error())
	}
	var objects []map[string]interface{}

	rows, err := db.Query(query, args...)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		// figure out what columns were returned
		// the column names will be the JSON object field keys
		columns, err := rows.ColumnTypes()
		if err != nil {
			return nil, err
		}

		// Scan needs an array of pointers to the values it is setting
		// This creates the object and sets the values correctly
		values := make([]interface{}, len(columns))
		object := map[string]interface{}{}
		for i, column := range columns {
			var v interface{}

			switch column.DatabaseTypeName() {
			case "DECIMAL":
				v = new(sql.NullString)
			case "VARCHAR":
				v = new(sql.NullString)
			case "CHAR":
				v = new(sql.NullString)
			case "BIGINT":
				v = new(sql.NullInt64)
			case "SMALLINT":
				v = new(sql.NullInt64)
			case "DATE":
				v = new(sql.NullString)
			case "TEXT":
				v = new(sql.NullString)
			case "LONGTEXT":
				v = new(sql.NullString)
			case "DATETIME":
				v = new(sql.NullString)
			case "INT":
				v = new(sql.NullInt64)
			case "VARBINARY":
				v = new(sql.NullString)
			case "DOUBLE":
				v = new(sql.NullString)
			case "MEDIUMINT":
				v = new(sql.NullInt64)
			default:
				v = new(interface{}) // destination must be a pointer

				// use to figure out types for columns
				log.Println(column.Name(), column.DatabaseTypeName())

			}

			object[column.Name()] = v

			values[i] = v

		}

		err = rows.Scan(values...)

		if err != nil {
			return nil, err
		}

		row := map[string]interface{}{}

		for k := range object {

			row[k] = ConvertBackToStandardType(object[k])
		}

		objects = append(objects, row)
	}

	if len(objects) == 0 {
		return objects, errors.New("Error No Records Found")
	}

	return objects, err
}

func ConvertBackToStandardType(v interface{}) interface{} {

	var output interface{}

	temptype := reflect.TypeOf(v).String()

	switch temptype {

	case "*sql.NullInt64":
		s := v.(*sql.NullInt64)
		return s.Int64

	case "*sql.NullString":
		s := v.(*sql.NullString)
		return s.String

	case "*sql.NullFloat64":
		s := v.(*sql.NullFloat64)
		return s.Float64
	}

	return output

}

func GetConnectionStringWithDb() string {
	return os.Getenv("MCS")
}

func GetConnectionStringNoDb(cs string) string {
	return strings.Split(cs, "/")[0] + "/"
}

func GetConnectionWithDb(cs string) *sql.DB {
	connectionWithDb, err := sql.Open("mysql", cs)
	if err != nil {
		fmt.Print(err.Error())
	}
	return connectionWithDb
}

func GenConnectionNoDb(cs string) *sql.DB {
	connectionNoDb, err := sql.Open("mysql", GetConnectionStringNoDb(cs))
	if err != nil {
		fmt.Print(err.Error())
	}
	return connectionNoDb
}
func NonQuery(cs, query string, args ...interface{}) error {
	con := GenConnectionNoDb(cs)

	_, err := con.Query(query, args...)

	if err != nil {
		return err
	}

	return nil

}

func NonQueryDb(cs, query string, args ...interface{}) (int64, error) {
	con := GetConnectionWithDb(cs)

	ccon, err := con.Exec(query, args...)

	if err != nil {
		return 0, err
	}

	id, err := ccon.LastInsertId()
	return id, nil
}
