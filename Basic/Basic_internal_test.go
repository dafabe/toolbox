package Basic

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"github.com/fatih/color"
	"os"
	"testing"
)

var cs = os.Getenv("MCS")

func TestMain(m *testing.M) {
	setup()
	code := m.Run()
	teardown()
	os.Exit(code)
}

func teardown() {

	//GenConnectionNoDb(cs).Query("DROP TABLE TEST_TAB")
	//GenConnectionNoDb(cs).Query("DROP DATABASE TESTDB")
}

func setup() {

	CreateDataBase()
	CreateTable()
	PopulateTable()
}
func ExampleNonQuery() {

	NonQuery(cs, "create database HELLO")
	fmt.Print("asdf")

	// Output: wer
}

func TestQueryToSliceOfMaps(t *testing.T) {

	allData, err := QueryToSliceOfMaps(cs, "select * from TEST_TAB where age = ?", 25)

	if err != nil {
		color.Red(err.Error())
	}

	expected := "Dave"

	actual := allData[0]["Name"]

	if expected != actual {
		t.Errorf("Expected: %s  Acutal:   %s", expected, actual)
	}

}

func TestQueryToMapOfMapsNil(t *testing.T) {

	allData, err := QueryToMapOfMaps("Name", cs, "select * from TEST_TAB where age = 100")

	if err != nil {
		color.Red(err.Error())
	}

	expected := err.Error()

	actual := "Error No Records Found"

	fmt.Print(allData)
	if expected != actual {
		t.Errorf("Expected: %s  Acutal:   %s", expected, actual)
	}

}

func TestQueryToMapOfMaps(t *testing.T) {

	allData, err := QueryToMapOfMaps("Name", cs, "select * from TEST_TAB")

	if err != nil {
		color.Red(err.Error())
	}

	expected := "Isaac"

	actual := allData["Isaac"]["Name"]

	fmt.Print(allData)
	if expected != actual {
		t.Errorf("Expected: %s  Acutal:   %s", expected, actual)
	}

}

func TestQueryToJsons(t *testing.T) {

	allData, err := QueryToJson(cs, "select * from TEST_TAB")

	if err != nil {
		color.Red(err.Error())
	}

	allData, err = json.MarshalIndent(allData, "", "    ")

	if err != nil {
		fmt.Print(err)
	}

	fmt.Print(allData)

}

func TestQueryToJsonsArgs(t *testing.T) {

	allData, err := QueryToJson(cs, "select * from TEST_TAB where age = ?", 25)

	if err != nil {
		color.Red(err.Error())
	}

	allData, err = json.MarshalIndent(allData, "", "    ")

	if err != nil {
		fmt.Print(err)
	}

	fmt.Print(allData)

}

func PopulateTable() {
	GetConnectionWithDb(cs).Query(`INSERT INTO TEST_TAB (Name, Age) VALUES ('Dave', 39);`)
	GetConnectionWithDb(cs).Query(`INSERT INTO TEST_TAB (Name, Age) VALUES ('Isaac', 25);`)
	GetConnectionWithDb(cs).Query(`INSERT INTO TEST_TAB (Name, Age) VALUES ('Iain', 30);`)

}

func CreateTable() {
	tabquery := `CREATE TABLE TEST_TAB (
						PersonID int NOT NULL AUTO_INCREMENT,
						Name varchar(255),
						Age int,
				        PRIMARY KEY (PersonID)
					);`
	GetConnectionWithDb(cs).Query(tabquery)
}

func CreateDataBase() (*sql.Rows, error) {
	return GenConnectionNoDb(cs).Query("CREATE DATABASE TESTDB")
}
func ExampleQueryToSliceOfMaps() {
	allData, err := QueryToSliceOfMaps(cs, "select * from TEST_TAB")

	if err != nil {
		fmt.Print(err.Error())
	}

	fmt.Print(allData)

	// Output:
	// [map[Age:39 Name:Dave PersonID:1] map[Age:25 Name:Isaac PersonID:2] map[Age:30 Name:Iain PersonID:3]]

}

func TestAddEntryRetId(t *testing.T) {
	cs = "root:rootroot@tcp(localhost:3306)/BONGO"
	fmt.Println(cs)
	GenConnectionNoDb(cs).Query("DROP DATABASE IF EXISTS BONGO")

	GenConnectionNoDb(cs).Query("CREATE DATABASE BONGO")

	query := `CREATE TABLE User (
    ID int NOT NULL AUTO_INCREMENT PRIMARY KEY,
    Name varchar(10)
);`
	_, err := NonQueryDb(cs, query)

	if err != nil {

	}
	query = "INSERT INTO user VALUES (?, ?); "

	_, err = NonQueryDb(cs, query, nil, "dave")

	if err != nil {

	}

	query = "INSERT INTO user VALUES (?, ?); "

	res, err := NonQueryDb(cs, query, nil, "dave")

	if err != nil {

	}

	expected := int64(2)

	id := res

	if err != nil {

	}
	actual := id

	if expected != actual {
		t.Errorf("Expected: %d  Acutal:   %d", expected, actual)
	}

}
