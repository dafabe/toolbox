package SmartTicker

import (
	"time"
)

type SmartTicker struct {
	InitialTime       time.Duration
	SubsequentTime    time.Duration
	InitialMessage    string
	SubsequentMessage string
	InitialAction     func(s string)
	SubsequentAction  func(s string)
	ConditionMet      func() bool
}

func NewSmartTicker(initialTime time.Duration, subsequentTime time.Duration, initialMessage string, subsequentMessage string, initialAction func(s string), subsequentAction func(s string), conditionMet func() bool) *SmartTicker {
	return &SmartTicker{InitialTime: initialTime, SubsequentTime: subsequentTime, InitialMessage: initialMessage, SubsequentMessage: subsequentMessage, InitialAction: initialAction, SubsequentAction: subsequentAction, ConditionMet: conditionMet}
}

// TODO how do we test for and ensure the this works after condition changes to false
func (s *SmartTicker) Start(time ITime) {

	time.SleepMin(s.InitialTime)

	if s.ConditionMet() {
		s.InitialAction(s.InitialMessage)
	}
	longer(s, time)

}

func longer(s *SmartTicker, time ITime) func() {
	if s.ConditionMet() {
		time.SleepMin(s.SubsequentTime)
		s.SubsequentAction(s.SubsequentMessage)
		return longer(s, time)
	}
	return nil
}

type ITime interface {
	Sleep(d time.Duration)
	SleepMin(d time.Duration)
}

type Time struct {
}

func (t Time) Sleep(d time.Duration) {
	time.Sleep(d)
}

func (t Time) SleepMin(d time.Duration) {
	time.Sleep(d * time.Minute)
}
