package Fd

import (
	"bufio"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
)

func ListFilesInfos(dir string) []os.FileInfo {
	files, err := ioutil.ReadDir(dir)
	if err != nil {
		log.Fatal(err)
	}
	return files
}

func ListFileNames(dir string) []string {
	files := ListFilesInfos(dir)
	names := []string{}

	for _, val := range files {
		names = append(names, val.Name())
	}
	return names
}

func ListFileFps(dir string) []string {
	files := ListFileNames(dir)
	fps := []string{}

	for _, val := range files {
		fps = append(fps, filepath.Join(dir, val))
	}
	return fps
}
func ListFileFpsDirsOnly(dir string) []string {
	files := ListFileNames(dir)
	fps := []string{}

	for _, val := range files {
		fp := filepath.Join(dir, val)
		if IsDir(fp) {

			fps = append(fps, fp)
		}
	}
	return fps
}
func IsDir(fp string) bool {

	fi, err := os.Stat(fp)
	if err != nil {
		fmt.Println(err)
		return false
	}
	switch mode := fi.Mode(); {
	case mode.IsDir():
		// do directory stuff
		return true
	case mode.IsRegular():
		// do file stuff
		return false
	}

	return false

}

type IDir interface {
	GetActualFiles()
}

type Dir struct {
	Dir string
}

func (d Dir) GetActualFiles(dir IDir) [][]byte {
	fps := ListFileFps(d.Dir)
	b := [][]byte{}

	for _, fp := range fps {
		readFile, err := ioutil.ReadFile(fp)
		if err != nil {
			log.Fatal(err)
		}
		b = append(b, readFile)

	}

	return b
}

func MakeDir(path string) {
	if _, err := os.Stat(path); os.IsNotExist(err) {
		os.MkdirAll(path, os.ModePerm)
	}
}

func RemDir(path string) error {
	err := os.RemoveAll(path)
	return err
}

func CreateFile(fp string) {
	if _, err := os.Stat(fp); os.IsNotExist(err) {
		d1 := []byte("This is a file!\n")
		err := ioutil.WriteFile(fp, d1, 0644)
		check(err)
	}

}
func WriteBytesToNewFile(fp string, b []byte) {
	if _, err := os.Stat(fp); os.IsNotExist(err) {
		fmt.Println("hello")
		err := ioutil.WriteFile(fp, b, 0644)
		check(err)
	}

}

func ReadBytesFromFile(fp string) ([]byte, error) {

	b, err := ioutil.ReadFile(fp)
	check(err)
	return b, nil

}

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func CopyFile(source string, dest string) (err error) {
	sourcefile, err := os.Open(source)
	if err != nil {
		return err
	}

	defer sourcefile.Close()

	destfile, err := os.Create(dest)
	if err != nil {
		return err
	}

	defer destfile.Close()

	_, err = io.Copy(destfile, sourcefile)
	if err == nil {
		sourceinfo, err := os.Stat(source)
		if err != nil {
			err = os.Chmod(dest, sourceinfo.Mode())
		}

	}

	return
}
func CopyDir(source string, dest string) (err error) {

	// get properties of source dir
	sourceinfo, err := os.Stat(source)
	if err != nil {
		return err
	}

	// create dest dir

	err = os.MkdirAll(dest, sourceinfo.Mode())
	if err != nil {
		return err
	}

	directory, _ := os.Open(source)

	objects, err := directory.Readdir(-1)

	for _, obj := range objects {

		sourcefilepointer := source + "/" + obj.Name()

		destinationfilepointer := dest + "/" + obj.Name()

		if obj.IsDir() {
			// create sub-directories - recursively
			err = CopyDir(sourcefilepointer, destinationfilepointer)
			if err != nil {
				fmt.Println(err)
			}
		} else {
			// perform copy
			err = CopyFile(sourcefilepointer, destinationfilepointer)
			if err != nil {
				fmt.Println(err)
			}
		}

	}
	return
}
func file() {

	// To start, here's how to dump a string (or just
	// bytes) into a file.
	d1 := []byte("hello\ngo\n")
	err := ioutil.WriteFile("/tmp/dat1", d1, 0644)
	check(err)

	// For more granular writes, open a file for writing.
	f, err := os.Create("/tmp/dat2")
	check(err)

	// It's idiomatic to defer a `Close` immediately
	// after opening a file.
	defer f.Close()

	// You can `Write` byte slices as you'd expect.
	d2 := []byte{115, 111, 109, 101, 10}
	n2, err := f.Write(d2)
	check(err)
	fmt.Printf("wrote %d bytes\n", n2)

	// A `WriteString` is also available.
	n3, err := f.WriteString("writes\n")
	fmt.Printf("wrote %d bytes\n", n3)

	// Issue a `Sync` to flush writes to stable storage.
	f.Sync()

	// `bufio` provides buffered writers in addition
	// to the buffered readers we saw earlier.
	w := bufio.NewWriter(f)
	n4, err := w.WriteString("buffered\n")
	fmt.Printf("wrote %d bytes\n", n4)

	// Use `Flush` to ensure all buffered operations have
	// been applied to the underlying writer.
	w.Flush()

}
