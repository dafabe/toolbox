package Break

import (
	"gitlab.com/dafabe/toolbox/Logger"
)

func Both(err error) {

	logger := Logger.GetLogger("log.json")

	logger.Info(err)

	return

}
