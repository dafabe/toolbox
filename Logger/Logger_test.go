package Logger

import (
	"errors"
	"fmt"
	"log"
	"net/smtp"
	"testing"
)

func DoString() (string, error) {
	return "my string", nil
}

func DoError() (string, error) {
	return "", errors.New("error ")
}

var Logger = GetLogger("mydlog.json")

func TestSend(t *testing.T) {
	fmt.Print("some stuff")

	SubFunc()

	fmt.Print("other stuff")

}

func SubFunc() {
	fmt.Print("Started...")
	s, err := DoString()

	if err != nil {
		Logger.Error(err)
		Logger.W
		return
	}

	fmt.Println(s)

	s, err = DoError()

	defer defFoo()
	HandleError(err)

	fmt.Println(s)
	fmt.Print("This should not have been reached")

}

func HandleError(err error) {
	if err != nil {
		Logger.Panic(err.Error())
	}

	return

}

func defFoo() {
	if r := recover(); r != nil {
		fmt.Println("Recover")
	}
}
